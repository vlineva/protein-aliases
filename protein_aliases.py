import json
import codecs
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import pandas as pd
import numpy as np
import datetime
import re
import os

sheets_dict = {
    'SingleProtein': 'group1',
    'Components' : 'group1',
    'ComplexProtein' : 'group2',
    'CORUM' : 'group2',
    'Non-proteins detected by Immunoassay' : 'group2',
    'ManuallyCuratedComplexes' : 'group2',
    'AssayRelatedProteinGroups' : 'group2'
}

group2_dict = {
    'ComplexProtein': {'id': '#Complex ac', 'bgName' : 'Recommended name', 'synonyms' : 'Aliases for complex'},
    'CORUM' : {'id': 'ComplexID', 'bgName' : 'ComplexName', 'synonyms' : 'Synonyms'},
    'Non-proteins detected by Immunoassay' : {'id': 'ID', 'bgName' : 'Name', 'synonyms' : 'Synonyms'},
    'ManuallyCuratedComplexes': {'id': 'ID', 'bgName' : 'ComplexName', 'synonyms' : ''},
    'AssayRelatedProteinGroups' : {'id': 'ID', 'bgName' : 'GroupName', 'synonyms' : ''}
}

def connect_to_sheet(credentials_json, gsheet_name):
    scope = [
            'https://spreadsheets.google.com/feeds', 
            'https://www.googleapis.com/auth/spreadsheets',
            'https://www.googleapis.com/auth/drive.file',
            'https://www.googleapis.com/auth/drive'
            ]
    creds = ServiceAccountCredentials.from_json_keyfile_name(credentials_json, scope)
    client = gspread.authorize(creds)
    sheet = client.open(gsheet_name)
    return sheet

def find_proteins_group1(protein_data, result):
    protein_dict = dict()
    recommended_names = protein_data[protein_data['type_of_name'] == 'recommendedName']
    short_name = recommended_names[recommended_names['subtype_of_name'] == 'shortName']
    
    if short_name.empty:
        main_info = recommended_names.iloc[0]
    else:
        main_info = short_name.iloc[0]
    protein_dict['uuid'] = main_info['uuid']
    
    if 'component#' in main_info.index:
        protein_dict['id'] = main_info['Uniprot'] + '_' + main_info['component#']
    else:
        protein_dict['id'] = main_info['Uniprot']
        
    protein_dict['bgName']  = main_info['name']
    protein_dict['synonyms'] = protein_data[protein_data['name'] != protein_dict['bgName']]['name'].to_list()
    result.append(protein_dict)

def find_proteins_group2(protein_data, sheet_dict, result):
    protein_dict = dict()
    protein_dict['uuid'] = protein_data['uuid']
    protein_dict['id'] = protein_data[sheet_dict['id']]
    protein_dict['bgName']  = protein_data[sheet_dict['bgName']]
    if sheet_dict['synonyms'] != '':
        protein_dict['synonyms'] = re.split('[;|\|]', protein_data[sheet_dict['synonyms']])
    else:
        protein_dict['synonyms'] = []
    result.append(protein_dict)

def add_proteins_to_json(path_to_json, credentials_json, uuid_to_find):
    for input_path in [path_to_json, credentials_json]:
        if not os.path.isfile(input_path):
            raise Exception('There is no file by path: {}'.format(input_path))
            
    with codecs.open(path_to_json, 'r', encoding='utf-8') as outfile:
        _json = json.load(outfile)

    #check uuid_to_find
    test_array = [np.nan, '', None]
    mask = np.isin(uuid_to_find, test_array, invert = True)
    uuid_to_find = np.array(uuid_to_find)[mask]
    #
    uuid_json = [subset['uuid'] for subset in _json['bgProteins']]
    uuid_to_find = np.setdiff1d(uuid_to_find, uuid_json)
    if uuid_to_find.shape[0] == 0:
        raise Exception('All these uuids are already in .json')

    google_sheet = connect_to_sheet(credentials_json = credentials_json, gsheet_name = 'BG_Proteins')

    result = []
    for sheet in google_sheet.worksheets():
        title = sheet.title
        if title in sheets_dict:
            data = sheet.get_all_values()
            headers = data.pop(0)
            df = pd.DataFrame(data, columns=headers)
            if sheets_dict[title] == 'group1':
                df[df['uuid'].isin(uuid_to_find)].groupby(by = 'Uniprot').apply(eval('find_proteins_group1'), (result))
            elif sheets_dict[title] == 'group2':
                sheet_dict = group2_dict[title]
                df[df['uuid'].isin(uuid_to_find)].apply(eval('find_proteins_group2'), args = [sheet_dict, result], axis = 1)
            else:
                print('Sheet {} is not in sheets_dict'.format(title))
    if not result:
        raise Exception('Can\'t find any uuids from input in BG_proteins')
    _json['bgProteins'] = _json['bgProteins'] + result
    _json['metadata']['createdAt'] = str(datetime.date.today())
    version_numbers = _json['metadata']['version'].split('.')
    _json['metadata']['version'] = '.'.join([version_numbers[0], str(int(version_numbers[1]) + 1)])

    with codecs.open(path_to_json, 'w', encoding = 'utf-8') as outfile:
        json.dump(_json, outfile, ensure_ascii = False)