## Installation

Use requirements.txt to install all necessary packages
```bash
pip install -r requirements.txt
```

## Usage

```python
import sys
sys.path.append('your/path/to/protein-aliases')
from protein_aliases import add_proteins_to_json

path_to_json = 'your/path/to/protein_aliases.json'
credentials_json = 'your/path/to/bg-proteins-creds.json'
add_proteins_to_json(path_to_json, credentials_json, uuid_to_find = ['46b9af94-12e1-4265-af32-37db799c9ae4', '70d559b5-7b93-406b-9dfa-747c40c6375d'])
```